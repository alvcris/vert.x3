package com.mozaiko.stock.dao;

import com.mozaiko.stock.model.Product;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * This is the DAO class for Products
 */
public class ProductDAO {
  private Map<Long, Product> products = new LinkedHashMap();

  public ProductDAO() {
    createDefaultProducts();
  }

  /**
   * Save a Product
   *
   * @param newProduct The new product to be added
   */
  public void save(final Product newProduct) {
    this.products.put(newProduct.getId(), newProduct);
  }

  /**
   * Get a list with all Products
   *
   * @return List<Product>
   */
  public List<Product> getAll() {
    return new ArrayList<>(products.values());
  }

  /**
   * Retrieve a Product according the given ID
   *
   * @param productID The productID to be searched
   * @return Optional<Product>
   */
  public Optional<Product> getProductById(final Long productID) {
    return Optional.ofNullable(products.get(productID));
  }

  /**
   * Retrieve a {@link List} of Products according the BarCode and Serial Number</br>
   *
   * @param product The Product to be compare
   * @return List<Product> Returns the product if matches or empty list otherwise
   */
  public List<Product> getProductsByBarCodeAndSerialNumber(final Product product) {
    return products.values().stream()
      .filter(retrievedProduct -> retrievedProduct.getSerialNumber() == product.getSerialNumber())
      .filter(retrievedProduct -> retrievedProduct.getBarCode().equals(product.getBarCode()))
      .collect(Collectors.toList());
  }

  /**
   * Update the current list of Products according the given Product
   *
   * @param product The product to be updated
   */
  public void updateProduct(final Product product) {
    products.replace(product.getId(), product);
  }

  private void createDefaultProducts() {
    IntStream.rangeClosed(1, 10).forEach(number -> {
      long id = 00001L;
      Product product = new Product(String.format("Samsung Galaxy S%s", number), id + number, number, false);
      this.products.put(product.getId(), product);
    });
  }
}
