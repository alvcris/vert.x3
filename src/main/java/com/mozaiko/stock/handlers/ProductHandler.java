package com.mozaiko.stock.handlers;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.AsyncResult;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents the handler the will send the eventBus and handles responses.
 */
@NoArgsConstructor
public class ProductHandler {
  private static final Logger LOGGER = LoggerFactory.getLogger(ProductHandler.class);

  /**
   * Save Product. Sends messages via EventBus and handles the return.
   * Returns Http status Conflict if the product is already saved.
   *
   * @param routingContext The application context
   * @param vertx          Vertx instance
   */
  public void save(final RoutingContext routingContext, final Vertx vertx) {
    String methodName = "save(): %s";
    LOGGER.info(String.format(methodName, "Entering"));
    JsonObject bodyAsJson = routingContext.getBodyAsJson();

    if (!validateBody(bodyAsJson)) {
      LOGGER.error(String.format(methodName, "All fields are mandatory"));
      routingContext.fail(HttpResponseStatus.BAD_REQUEST.code());
      return;
    }

    vertx.eventBus().send("SAVE_PRODUCT", bodyAsJson, (AsyncResult<Message<JsonObject>> result) -> {


      if (result.result().body().isEmpty()) {
        LOGGER.error(String.format(methodName, "Conflict"));
        routingContext.fail(HttpResponseStatus.CONFLICT.code());
        return;
      }

      createResponse(routingContext, result.result().body(), HttpResponseStatus.CREATED.code());
    });
    LOGGER.info(String.format(methodName, "Exiting with Status 200"));
  }

  /**
   * Get all products via eventBus messages
   *
   * @param routingContext The application context
   * @param vertx          Vertx instance
   */
  public void getAll(final RoutingContext routingContext, final Vertx vertx) {
    String methodName = "getAll(): %s";
    LOGGER.info(String.format(methodName, "Entering"));
    vertx.eventBus().send("GET_ALL_PRODUCTS",
      new JsonObject(), (AsyncResult<Message<JsonObject>> result) -> createResponse(routingContext, result.result().body(), HttpResponseStatus.OK.code())
    );
    LOGGER.info(String.format(methodName, "Exiting"));
  }

  /**
   * Get one Product via eventBus message
   * Returns Http status 404 if the Product was not found
   *
   * @param routingContext The application context
   * @param vertx          Vertx instance
   */
  public void getOneProduct(final RoutingContext routingContext, final Vertx vertx) {
    String methodName = "getAll(): %s";
    LOGGER.info(String.format(methodName, "Entering"));
    vertx.eventBus().send("GET_PRODUCT", createJsonMessageFromParam(routingContext.request().getParam("productID")), (AsyncResult<Message<JsonObject>> result) -> {

      if (result.result().body().isEmpty()) {
        LOGGER.error(String.format(methodName, "Could not find the requested resource."));
        routingContext.fail(HttpResponseStatus.NOT_FOUND.code());
        return;
      }

      createResponse(routingContext, result.result().body(), HttpResponseStatus.OK.code());
    });
    LOGGER.info(String.format(methodName, "Exiting"));
  }

  /**
   * Update a product via eventBus messages
   * Returns Http status 404 if the Product was not found
   *
   * @param routingContext The application context
   * @param vertx          Vertx instance
   */
  public void updateProduct(final RoutingContext routingContext, final Vertx vertx) {
    String methodName = "updateProduct(): %s";
    LOGGER.info(String.format(methodName, "Entering"));
    JsonObject bodyAsJson = routingContext.getBodyAsJson();

    if (!validateBody(bodyAsJson)) {
      LOGGER.error(String.format(methodName, "All fields are mandatory"));
      routingContext.fail(HttpResponseStatus.BAD_REQUEST.code());
      return;
    }

    JsonObject jsonObject = createJsonMessageFromParam(routingContext.request().getParam("productID"));
    jsonObject.put("newProduct", bodyAsJson);

    vertx.eventBus().send("UPDATE_PRODUCT", jsonObject, (AsyncResult<Message<JsonObject>> result) -> {

      if (result.result().body().isEmpty()) {
        LOGGER.error(String.format(methodName, "Could not find the requested resource."));
        routingContext.fail(HttpResponseStatus.NOT_FOUND.code());
        return;
      }

      if (result.result().body().containsKey("conflict")) {
        LOGGER.error(String.format(methodName, result.result().body().getString("conflict")));
        routingContext.fail(HttpResponseStatus.CONFLICT.code());
        return;
      }
      createResponse(routingContext, result.result().body(), HttpResponseStatus.OK.code());
    });
    LOGGER.info(String.format(methodName, "Exiting"));
  }

  private boolean validateBody(final JsonObject bodyAsJson) {
    return bodyAsJson.containsKey("name") && StringUtils.isNotEmpty(bodyAsJson.getString("name"))
      && bodyAsJson.containsKey("barCode") && bodyAsJson.getLong("barCode") > 0
      && bodyAsJson.containsKey("serialNumber") && bodyAsJson.getInteger("serialNumber") > 0;
  }

  private void createResponse(final RoutingContext routingContext, final JsonObject message, final int code) {
    routingContext.response()
      .putHeader("Content-Type", "application/json")
      .setStatusCode(code)
      .end(message.encodePrettily());
  }

  private JsonObject createJsonMessageFromParam(final String productId) {
    return new JsonObject().put("productID", Integer.parseInt(productId));
  }
}
