package com.mozaiko.stock.model;

import lombok.Getter;
import lombok.Setter;

import java.util.concurrent.atomic.AtomicLong;

/**
 * This class represents The Product
 */
@Getter
@Setter
public class Product {
  private static final AtomicLong COUNTER = new AtomicLong(0);
  private Long id;
  private String name;
  private Long barCode;
  private Integer serialNumber;
  private boolean sold;


  public Product(final String name, final Long barCode, final Integer serialNumber, final boolean sold) {
    this.id = COUNTER.getAndIncrement();
    this.serialNumber = serialNumber;
    this.name = name;
    this.barCode = barCode;
    this.sold = sold;
  }

  public Product() {
    this.id = COUNTER.getAndIncrement();
  }
}
