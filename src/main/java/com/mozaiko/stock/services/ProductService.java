package com.mozaiko.stock.services;


import com.mozaiko.stock.dao.ProductDAO;
import com.mozaiko.stock.model.Product;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * This class represents the Service layer in order to manipulate Products
 */
@AllArgsConstructor
@NoArgsConstructor
public class ProductService {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProductService.class);
  private ProductDAO productDAO;

  public void getAll(final Message<JsonObject> message) {
    String methodName = "getAll(): %s";
    LOGGER.info(String.format(methodName, "Entering"));

    List<Product> all = productDAO.getAll();
    JsonObject result = createJsonArrayResult(all);
    message.reply(result);
    LOGGER.info(String.format(methodName, "Exiting - Got all Products"));
  }

  /**
   * Save Product.
   * Reply the message with the saved Product or an empty message in case of error
   *
   * @param message The message will represent the Product to be saved
   */
  public void save(final Message<JsonObject> message) {
    String methodName = "save(): %s";
    LOGGER.info(String.format(methodName, "Entering"));

    Product newProduct = Json.decodeValue(message.body().toString(), Product.class);

    List<Product> products = productDAO.getProductsByBarCodeAndSerialNumber(newProduct);

    if (products.isEmpty()) {
      LOGGER.info(String.format(methodName, "Saving product with ID " + newProduct.getId()));
      productDAO.save(newProduct);
      message.reply(createJsonObjectResult(newProduct));
      return;
    }
    message.reply(new JsonObject());
    LOGGER.info(String.format(methodName, "Exiting"));
  }

  /**
   * Retrieve the given product
   * Reply with the found Product or an empty message if not found
   *
   * @param message The message will represent the Product to be found
   */
  public void getProduct(final Message<JsonObject> message) {
    String methodName = "getProduct(): %s";
    LOGGER.info(String.format(methodName, "Entering"));

    Optional<Product> product = productDAO.getProductById(message.body().getLong("productID"));

    if (product.isPresent()) {
      LOGGER.info(String.format(methodName, "Retrieving product with ID " + product.get().getId()));
      message.reply(createJsonObjectResult(product.get()));
      return;
    }
    message.reply(new JsonObject());
    LOGGER.info(String.format(methodName, "Exiting with empty message, no product was found!"));
  }

  /**
   * This method will update the current Product as given
   * Reply with the updated Product if found or updated
   * Reply with an empty message if the product was not found
   *
   * @param message The message will represent the Product to be updated
   */
  public void update(final Message<JsonObject> message) {
    String methodName = "update(): %s";
    LOGGER.info(String.format(methodName, "Entering"));

    Long productID = message.body().getLong("productID");
    Optional<Product> product = productDAO.getProductById(productID);

    if (product.isPresent()) {
      LOGGER.info(String.format(methodName, "Product " + product.get().getId() + " was found"));

      JsonObject newProductJsonObject = message.body().getJsonObject("newProduct");
      Product newProduct = Json.decodeValue(newProductJsonObject.toString(), Product.class);
      newProduct.setId(productID);

      List<Product> products = productDAO.getProductsByBarCodeAndSerialNumber(newProduct);

      if (products.isEmpty()) {
        LOGGER.info(String.format(methodName, "Updating Product"));
        productDAO.updateProduct(newProduct);
        message.reply(createJsonObjectResult(newProduct));
        LOGGER.info(String.format(methodName, "Product was updated"));
        return;
      }
      LOGGER.info(String.format(methodName, "This Product already exist, so attempting to update only the name and sold aspect"));
      Optional<Product> updatedProduct = products.stream()
        .filter(filteredProduct -> filteredProduct.getId().equals(newProduct.getId()))
        .peek(filteredProduct -> {
          filteredProduct.setName(newProduct.getName());
          filteredProduct.setSold(newProduct.isSold());
        }).findFirst();

      if (updatedProduct.isPresent()) {
        productDAO.updateProduct(updatedProduct.get());
        message.reply(createJsonObjectResult(updatedProduct.get()));
        LOGGER.info(String.format(methodName, "Product was updated"));
        return;
      }

      String errorMessage = "Conflict, it seems that there is another product that matches. Products found = [" + Arrays.toString(products.toArray()) + "]";
      LOGGER.error(String.format(methodName, errorMessage));
      message.reply(createJsonObjectResultWithKeyAndValue("conflict", errorMessage));
      return;

    }
    message.reply(new JsonObject());
    LOGGER.info(String.format(methodName, "Exiting with empty message, no product was found!"));
  }

  private JsonObject createJsonObjectResultWithKeyAndValue(final String key, final String value) {
    JsonObject result = new JsonObject();
    result.put(key, value);
    return result;
  }

  private JsonObject createJsonArrayResult(List<Product> products) {
    JsonObject result = new JsonObject();
    result.put("result", new JsonArray(Json.encode(products)));
    return result;
  }

  private JsonObject createJsonObjectResult(final Product updatedProduct) {
    JsonObject result = new JsonObject();
    result.put("result", new JsonObject(Json.encode(updatedProduct)));
    return result;
  }
}
