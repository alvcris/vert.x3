package com.mozaiko.stock.starter;

import io.vertx.core.Launcher;

public class ApplicationLauncher {

  public static void main(String[] args) {
    Launcher.main(new String[]{"run", StockStarter.class.getName(),
      "-Dvertx.options.maxWorkerExecuteTime=30000000000000"});
  }
}
