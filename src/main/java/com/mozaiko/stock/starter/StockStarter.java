package com.mozaiko.stock.starter;

import com.mozaiko.stock.handlers.ProductHandler;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.ErrorHandler;

import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;

/**
 * This is the main Verticle it will configure and start the application
 */
public class StockStarter extends AbstractVerticle {

  private static final String APPLICATION_JSON = "application/json";
  private ProductHandler productHandler;

  @Override
  public void start(final Future<Void> future) throws Exception {
    vertx.deployVerticle("com.mozaiko.stock.verticles.ProductWorkerVerticle",
      new DeploymentOptions().setWorker(true).setInstances(1));

    productHandler = new ProductHandler();
    HttpServer server = vertx.createHttpServer(createOptions());
    Router router = createEndpoints(createRouter());
    server.requestHandler(router);
    server.listen(result -> {
      if (result.succeeded()) {
        future.complete();
      } else {
        future.fail(result.cause());
      }
    });
  }

  private Router createEndpoints(final Router router) {
    router.get("/products").handler(routingContext -> productHandler.getAll(routingContext, vertx));
    router.get("/products/:productId").handler(routingContext -> productHandler.getOneProduct(routingContext, vertx));
    router.put("/products/:productId").handler(routingContext -> productHandler.updateProduct(routingContext, vertx));
    router.post("/products").handler(routingContext -> productHandler.save(routingContext, vertx));
    return router;
  }

  private Router createRouter() {
    Router router = Router.router(vertx);
    router.route().failureHandler(ErrorHandler.create(true));
    router.route().consumes(APPLICATION_JSON);
    router.route().produces(APPLICATION_JSON);
    router.route().handler(BodyHandler.create());
    router.route().handler(context -> {
      context.response().headers().add(CONTENT_TYPE, APPLICATION_JSON);
      context.next();
    });
    return router;
  }

  private HttpServerOptions createOptions() {
    HttpServerOptions options = new HttpServerOptions();
    options.setHost("localhost");
    options.setPort(8080);
    return options;
  }
}
