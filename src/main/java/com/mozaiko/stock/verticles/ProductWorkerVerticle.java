package com.mozaiko.stock.verticles;

import com.mozaiko.stock.dao.ProductDAO;
import com.mozaiko.stock.services.ProductService;
import io.vertx.core.AbstractVerticle;

/**
 * This class represents the Verticle for the ProductWorker. It will create all consumers for the eventBus
 */
public class ProductWorkerVerticle extends AbstractVerticle {

  @Override
  public void start() throws Exception {

    ProductDAO productDAO = new ProductDAO();

    ProductService productService = new ProductService(productDAO);

    vertx.eventBus().consumer("GET_ALL_PRODUCTS", productService::getAll);

    vertx.eventBus().consumer("GET_PRODUCT", productService::getProduct);

    vertx.eventBus().consumer("SAVE_PRODUCT", productService::save);

    vertx.eventBus().consumer("UPDATE_PRODUCT", productService::update);
  }
}
