package com.mozaiko.stock.integration;

import com.mozaiko.stock.model.Product;
import com.mozaiko.stock.response.ProductResponse;
import com.mozaiko.stock.starter.StockStarter;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.net.ServerSocket;

@RunWith(VertxUnitRunner.class)
public class StockStarterVerticleTest {

  private static Vertx vertx;
  private static Integer port;

  @BeforeClass
  public static void deployVerticle(TestContext context) throws IOException {
    vertx = Vertx.vertx();
    ServerSocket socket = new ServerSocket(8080);
    port = socket.getLocalPort();
    socket.close();

    DeploymentOptions options = new DeploymentOptions()
      .setConfig(new JsonObject().put("http.port", port)
      );

    vertx.deployVerticle(StockStarter.class.getName(), options, context.asyncAssertSuccess());
  }

  @AfterClass
  public static void tearDown(TestContext context) {
    vertx.close(context.asyncAssertSuccess());
  }

  @Test
  public void verifyTheProductAdditionShouldReturnANewProduct(TestContext context) {
    final Async async = context.async();
    final String json = Json.encodePrettily(new Product("Product1", 001L, 002, false));

    vertx.createHttpClient().post(port, "localhost", "/products")
      .putHeader("content-type", "application/json")
      .putHeader("content-length", Integer.toString(json.length()))
      .handler(response -> {
        context.assertEquals(response.statusCode(), 201);
        response.bodyHandler(body -> {
          final ProductResponse productResponse = Json.decodeValue(body.toString(), ProductResponse.class);
          context.assertEquals(productResponse.getProduct().getName(), "Product1", "Product name does not match");
          context.assertEquals(productResponse.getProduct().getBarCode(), 001L, "Product barCode does not match");
          context.assertEquals(productResponse.getProduct().getSerialNumber(), 002, "Product serial number does not match");
          context.assertNotNull(productResponse.getProduct().getId(), "Product ID should not be null");
          async.complete();
        });
      })
      .write(json)
      .end();
  }

  @Test
  public void addProductShouldReturnConflictWhenAddingAProductTwice(TestContext context) {
    final Async async = context.async();
    final String json = Json.encodePrettily(new Product("Product1", 2L, 1, false));

    vertx.createHttpClient().post(port, "localhost", "/products")
      .putHeader("content-type", "application/json")
      .putHeader("content-length", Integer.toString(json.length()))
      .handler(response -> {
        context.assertEquals(response.statusCode(), 409);
        async.complete();
      })
      .write(json)
      .end();
  }

  @Test
  public void shouldReturnConflictWhenEditingAProductThatAlreadyExistsWithOtherID(TestContext context) {
    final Async async = context.async();
    final String json = Json.encodePrettily(new Product("Product1", 11L, 10, false));

    vertx.createHttpClient().put(port, "localhost", "/products/1")
      .putHeader("content-type", "application/json")
      .putHeader("content-length", Integer.toString(json.length()))
      .handler(response -> {
        context.assertEquals(response.statusCode(), 409);
        async.complete();
      })
      .write(json)
      .end();
  }

  @Test
  public void editProductShouldReturnConflictWhenUpdatingAProductThatAlreadyExists(TestContext context) {
    final Async async = context.async();
    final String json = Json.encodePrettily(new Product("Product1", 2L, 1, false));

    vertx.createHttpClient().put(port, "localhost", "/products/1")
      .putHeader("content-type", "application/json")
      .putHeader("content-length", Integer.toString(json.length()))
      .handler(response -> {
        context.assertEquals(response.statusCode(), 409);
        async.complete();
      })
      .write(json)
      .end();
  }

  @Test
  public void editProductShouldAllowEditionOnlyToTheProductNameWhenDetectingTheProductAlreadyExistWithTheSameID(TestContext context) {
    final Async async = context.async();
    final String json = Json.encodePrettily(new Product("Product1", 2L, 1, false));

    vertx.createHttpClient().put(port, "localhost", "/products/0")
      .putHeader("content-type", "application/json")
      .putHeader("content-length", Integer.toString(json.length()))
      .handler(response -> {
        context.assertEquals(response.statusCode(), 200);
        response.bodyHandler(body -> {
          final ProductResponse productResponse = Json.decodeValue(body.toString(), ProductResponse.class);
          context.assertEquals(productResponse.getProduct().getName(), "Product1", "Product name does not match");
          context.assertEquals(productResponse.getProduct().getBarCode(), 2L, "Product barCode does not match");
          context.assertEquals(productResponse.getProduct().getSerialNumber(), 1, "Product serial number does not match");
          context.assertNotNull(productResponse.getProduct().getId(), "Product ID should not be null");
          async.complete();
        });
      })
      .write(json)
      .end();
  }

  @Test
  public void shouldReturnBadRequestWhenPostingMissingAMandatoryField(TestContext context) {
    final Async async = context.async();
    final String json = Json.encodePrettily(new Product("", 2L, 1, false));

    vertx.createHttpClient().post(port, "localhost", "/products/")
      .putHeader("content-type", "application/json")
      .putHeader("content-length", Integer.toString(json.length()))
      .handler(response -> {
        context.assertEquals(response.statusCode(), 400);
        async.complete();
      })
      .write(json)
      .end();
  }

  @Test
  public void shouldReturnBadRequestWhenPuttingMissingAMandatoryField(TestContext context) {
    final Async async = context.async();
    final String json = Json.encodePrettily(new Product("", 2L, 1, false));

    vertx.createHttpClient().put(port, "localhost", "/products/1")
      .putHeader("content-type", "application/json")
      .putHeader("content-length", Integer.toString(json.length()))
      .handler(response -> {
        context.assertEquals(response.statusCode(), 400);
        async.complete();
      })
      .write(json)
      .end();
  }
}
