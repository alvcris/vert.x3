package com.mozaiko.stock.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mozaiko.stock.model.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class ProductResponseList {

  @JsonProperty("result")
  private List<Product> products;
}
